# Example App

Here is an non-working example of how I would like to build desktop applications using only HTML CSS and JavaScript.

Within a [style.css](./style.css), you'll see `html{}` defines the window size and position exactly how you would in native CSS.

The application will always initially boot [index.html](./index.html)

I also propose a Bridge API that gives the application useful functions to control Native Desktop API's.

Example method to build and run the app would be to use the command line.

`make run` copies relative bridge files and launches the application [makefile](./makefile).


